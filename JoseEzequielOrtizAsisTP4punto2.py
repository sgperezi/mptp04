import os
########################################################################
def menu(valoresCargados):
    #    '''El módulo devuelve un valor entero, validando que se encuentre en el intervalo [1,6]'''
    print('Ingresa la letra correspondiente a la funcionalidad que quieras probar:')
    opcion=input('''
    (a) Registrar productos.
    (b) Listado de productos
    (c) Buscar productos por cantidad de Stock con Intervalo
    (d) Aumentar Stock de Productos con cantidades menores a Y
    (e) Eliminar Productos con 0 (cero) Stock
    (f) Salir)\nOPCION:...''')
    
    if opcion in ['a','b','c','d','e','f']:
        return opcion
    else:
        os.system ("cls")
        print('Opcion Incorrecta\nINTENTA DE NUEVO') 
############################################################################################################
def ingresarSTOCK():
    ingreso=False
    while ingreso==False:
        try:
            A=int (input(f'Ingresa el STOCK: '))
            if A >= 0:
                ingreso=True
                return A
            else:
                print("ERROR. El valor de STOCK debe ser numerico")
        except:
            os.system ("cls") 
            print('UPS!...INTENTA DE NUEVO\nEl valor de STOCK debe ser numerico y entero') 
############################################################################################################
def aumentarSTOCK():
    ingreso=False
    while ingreso==False:
        try:
            A=int (input(f'Ingresa el STOCK: '))
            if A > 0:
                ingreso=True
                return A
            elif A==0:
                print('No se realizara ningun aumento')
                return 0
        except:
            os.system ("cls") 
            print('UPS!...INTENTA DE NUEVO\nEl valor de STOCK debe ser numerico, entero') 
############################################################################################################
def ingresarPrecio():
    ingreso=False
    while ingreso==False:
        try:
            A=float (input(f'Ingresa el Precio: '))
            if A > 0:
                ingreso=True
                return A
            else:
                print("ERROR. El valor de PRECIO debe ser numerico y mayor a cero")
        except:
            os.system ("cls") 
            print('UPS!...INTENTA DE NUEVO\nEl valor de PRECIO debe ser numerico y mayor a cero') 
############################################################################################################
def ingresarEntero():
    ingreso=False
    while ingreso==False:
        try:
            A=int (input(f'Ingresa un codigo para el producto: '))
            ingreso=True
            return A
        except:
            os.system ("cls") 
            print('UPS!...INTENTA DE NUEVO\nEl valor debe ser numerico y entero') 
############################################################################################################
def ingresarDescripcion():
            A=input(f'Ingresa una descripcion para el producto: ')
            return A
############################################################################################################
def datosDeProductos():
    print('DATOS REQUERIDOS DEL PRODUCTO:')
############################################################################################################
def registrarProductos(dicProductos):
    respuesta='S'
    while respuesta=='S':
        codigo=ingresarEntero()
        if codigo in dicProductos:
            print('ERROR. Un producto ya se encuentra registrado con el mismo codigo. Intente otro Codigo')
        else:
            datosDeProductos()
            descripcion=ingresarDescripcion()
            precio=ingresarPrecio()
            stock=ingresarSTOCK()
            dicProductos [codigo]=[descripcion,precio,stock]
            os.system ("cls") 
            print('PRODUCTO REGISTRADO CON EXITO')
            respuesta=input('¿Desea ingresar otro producto? S/N\n')
############################################################################################################
def mostrarListadoDeProductos(dicProductos):
    os.system ("cls")
    print('CODIGO : Descripcion, Precio, STOCK')
    for codigo,datos in dicProductos.items():
        print(codigo,'    :',datos)
############################################################################################################
def mostrarListadoDeProductosPorIntervalo(dicProductos):
    os.system ("cls")
    print('Ingrese la cantidad Base de Stock para la busqueda')
    Base=ingresarSTOCK()
    print('Ingrese la cantidad Tope de Stock para la busqueda')
    Tope=ingresarSTOCK()
    if Tope<Base:
        return print('Error el Tope de Stock debe ser mayor a la Base.\nSe regresará al Menu Principal e intentelo de nuevo')
    else: 
        print('CODIGO : Descripcion, Precio, STOCK')
    for codigo,datos in dicProductos.items():
        if datos[2] in range(Base,Tope):
            print(codigo,'    :',datos)
############################################################################################################
def aumentarStockMenorQueY(dicProductos):
    os.system ("cls")
    print('Ingrese el valor de referencia Y')
    Y=ingresarSTOCK()
    print('Cuanto desea aumentar de Stock en los productos encontrados')
    X=aumentarSTOCK()
    for codigo,datos in dicProductos.items():
        if datos[2] < Y:
            datos[2]+=X
    print('AUMENTO DE STOCK CON EXITO')
############################################################################################################
def eliminarStockCero(dicProductos):
    CodigosAeliminar=[] 
    for codigo,datos in dicProductos.items():
        if datos[2]==0:
            CodigosAeliminar.append(codigo)  
    for i in CodigosAeliminar:
        del dicProductos[i] 
    print('Limpieza de Stock Realizada')
############################################################################################################
def salir():
    'Módulo que muestra un mensaje de despedida y anuncio de cierre del programa'
    from sys import exit
    print('El programa finalizara su ejecución.\n GRACIAS')
    exit()
############################################################################################################

os.system ("cls")
print('''*************************************************************
                INICIO DEL GESTIONADOR DE PRODUCTOS
**********************************************************************''')
opcion=0
dicProductos={}
valoresCargados=False
while opcion !='f':
    opcion=menu(valoresCargados)
    #os.system ("cls")
    if opcion in ['a','b','c','d','e','f']:
        os.system ("cls")
        print(f'ingresaste la opcion {opcion}')
        if opcion=='a':

            registrarProductos(dicProductos)

        elif opcion=='b':

            mostrarListadoDeProductos(dicProductos)

        elif opcion=='c':

            mostrarListadoDeProductosPorIntervalo(dicProductos)

        elif opcion=='d':

            aumentarStockMenorQueY(dicProductos)

        elif opcion=='e':
            
            eliminarStockCero(dicProductos)

        elif opcion=='f':
            salir()
